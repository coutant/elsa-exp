% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/eval_utils_d8c1_sc1a.R
\name{sdPerDataset}
\alias{sdPerDataset}
\title{Print per (cell, ligand) pair AUC sds of a DREAM 8 invivo solutions set}
\usage{
sdPerDataset(results)
}
\arguments{
\item{results}{the content of a DREAM 8 invivo evaluation file, obtained with \code{evaluateD8C1SC1ExperimentsFolderAll} and related functions}
}
\description{
Print per (cell, ligand) pair AUC sds of a DREAM 8 invivo solutions set
}
\seealso{
\code{evaluateD8C1SC1ExperimentsFolderAll}
}
