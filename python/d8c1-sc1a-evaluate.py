## Script to directly retrieve specific (cell, ligand) dreamtools score
## for DREAM 8 SC1A challenge

from dreamtools.dream8.D8C1 import scoring
import sys
import string

# Display feedback and get args: input file (containing models), cell, ligand, output file
print "evalD8C1A"
inputFilename = sys.argv[1]
cellLine = sys.argv[2].upper()
ligand = sys.argv[3].upper()
if ligand == "SERUM": ligand = "Serum"
elif ligand == "INSULIN": ligand = "Insulin"
outputFilename = sys.argv[4]

# Build the adequate dream8 scorer
s = scoring.HPNScoringNetwork(inputFilename)

# Launch the computation of all (cell, ligand) AUCs, then retrieve the one of interest
s.compute_all_aucs()
print "ALL AUCS"
print s.auc
ligandIndex = s.auc[cellLine].keys().index(ligand)
print "D8C1 SC1A AUC(", cellLine, ",", ligand, ") = ", s.auc[cellLine][ligand]
print s.descendancy_matrices[cellLine][ligand][s.mTor_index[cellLine]]
print s.edge_scores[cellLine][ligand][s.mTor_index[cellLine]]

# Write result in output file for easy retrieval from R afterwards
with open(outputFilename, "w") as f:
  f.write(`s.auc[cellLine][ligand]`)
