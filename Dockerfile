FROM debian:stable

RUN apt-get update
RUN apt-get install -y r-base r-base-dev python-pip python-tk libcurl4-gnutls-dev openssl git libssl-dev libxml2 libxml2-dev libcairo2-dev
RUN Rscript -e "install.packages(c(\"entropy\", \"igraph\", \"parallel\", \"pryr\", \"plyr\", \"dplyr\", \"reshape2\", \"mclust\", \"ggplot2\", \"devtools\", \"bnlearn\", \"R.cache\", \"data.table\", \"Rcpp\", \"roxygen2\", \"visNetwork\"), repos=\"http://cran.us.r-project.org\", dependencies=TRUE)"

RUN pip install cython
RUN pip install dreamtools

#TO CHANGE WITH ADEQUATE SYNAPSE CREDENTIALS
ARG SYNLOGIN
ARG SYNPWD
#ENV SYNLOGIN johndoe
#ENV SYNPWD mypwd

RUN touch $HOME/.synapseConfig
RUN echo "[authentication]\nusername: $SYNLOGIN\npassword: $SYNPWD" > $HOME/.synapseConfig
RUN ls -al $HOME
RUN cat $HOME/.synapseConfig
RUN echo $HOME
RUN dreamtools --challenge D8C1 --download-gold-standard
RUN dreamtools --challenge D8C1 --download-template

ADD ./elsa $HOME/elsa
ADD . $HOME/elsaexp
RUN rm -f $HOME/elsa/src/*.so
RUN rm -f $HOME/elsa/src/*.o
RUN rm -f $HOME/elsa/src/*.dll

CMD ["bash"]