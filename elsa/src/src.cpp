#include <Rcpp.h>
#include "OptimumBranching.hpp"
using namespace Rcpp;

// This is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar). Learn
// more about Rcpp at:
//
//   http://www.rcpp.org/
//   http://adv-r.had.co.nz/Rcpp.html
//   http://gallery.rcpp.org/
//

// [[Rcpp::export]]
Rcpp::DataFrame fastEdmonds(std::vector<int> froms, std::vector<int> tos, std::vector<double> weights, bool addDummyRoot){
  //Indices in froms and tos should begin to 1 (will be offset to 0) !!
  int maxElem = std::max(*std::max_element(froms.begin(), froms.end()), *std::max_element(tos.begin(), tos.end()));
  int numElem = addDummyRoot ? maxElem + 1 : maxElem;
  int sumElem = std::accumulate(weights.begin(), weights.end(), 0);
  std::list<std::pair<int, int> > edges;
  unsigned int offset = addDummyRoot ? 0 : -1; //Remove 1 for 1->0 indexing, and add 1 if dummy root to add
  for(unsigned int i = 0; i < froms.size(); ++i){
    edges.push_back(std::make_pair<int, int>(froms[i] + offset, tos[i] + offset));
  }
  std::list<double> ww(weights.begin(), weights.end());
  if(addDummyRoot){
    for(unsigned int i = 1; i <= maxElem; ++i){
      edges.push_back(std::make_pair<int, int>(0, i));
      ww.push_back(sumElem);
    }
  }
  std::list<std::tuple<int, int, double> > result;
  optimumBranching(edges, ww, numElem, result);
  std::list<int> resultFroms;
  std::list<int> resultTos;
  std::list<double> resultWeights;
  for(std::list<std::tuple<int, int, double> >::const_iterator r = result.begin(); r != result.end(); ++r){
    if(std::get<0>(*r) > 0 || !addDummyRoot){
      resultFroms.push_back(std::get<0>(*r) - offset);
      resultTos.push_back(std::get<1>(*r) - offset);
      resultWeights.push_back(std::get<2>(*r));
    }
  }
  return(Rcpp::DataFrame::create(Rcpp::Named("from") = wrap(resultFroms),
                                 Rcpp::Named("to") = wrap(resultTos),
                                 Rcpp::Named("weight") = wrap(resultWeights)));
}

// [[Rcpp::export]]
NumericVector fastBipartiteEdgeIds(Rcpp::CharacterVector nodesFrom, Rcpp::CharacterVector nodesTo){
  unsigned int toLen = nodesTo.size();
  unsigned int fromLen = nodesFrom.size();
  unsigned int allLen = toLen + fromLen;
  Rcpp::NumericVector res(toLen * fromLen);
  unsigned long k = 0;
  for(unsigned int j = 0; j <= fromLen - 1; ++j){
    for(unsigned int i = fromLen; i <= allLen - 1; ++i){
      res[k++] = j * allLen + i + 1;
    }
  }
  return(res);
}

// [[Rcpp::export]]
IntegerVector fastIndicesTable(IntegerVector numWithDuplicates){
  unsigned long resultLen = *(std::max_element(numWithDuplicates.begin(), numWithDuplicates.end()));
  unsigned long numLen = numWithDuplicates.size();
  Rcpp::IntegerVector res(resultLen);
  for(unsigned long k = 0; k < numLen; ++k){
    ++res[numWithDuplicates[k] - 1];
  }
  return(res);
}

/*Edgelist.bipartite.all = function(nodesFrom, nodesTo){
  toLen = length(nodesTo)
  fromLen = length(nodesFrom)
  allLen = sum(toLen, fromLen)
  toSeq = seq(0, fromLen - 1)
  fromSeq = seq(fromLen, allLen - 1)
  k = 1
  edgeIds = vector(mode = "integer", length = length(fromSeq) * length(toSeq))
  for(i in fromSeq){
    for(j in toSeq){
      edgeIds[k] = j * allLen + i + 1
      k = k + 1
    }
  }*/

// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R
timesTwo(42)
*/
