library(methods)

#'an S4 compact attributed edgelist pool representation
#'
#'This class allows the representation of a set of compact edgelists, with nodes names sequence shared between all embedded edgelists, and a list of edge index vectors, one for each embedded edgelist, using the same node names sequence order.
#'This class can be seen as a pool version of Edgelist, with no node names duplication, for maximal compactness.
#'
#'@slot nodes the character vector of node names
#'@slot edgeIds the list, with one element per embedded edgelist consisting in a numeric vector of edge indices. Indices maps to endpoints using the \code{nodes} order
#'@name EdgesPool class
#'@rdname EdgesPool-class
#'@export
#'
EdgesPool <- setClass(
  "EdgesPool", 
  slots = c(
    nodes = "character",
    edgeIds = "list"
  ),
  validity=function(object)
  {
    for(eIds in object@edgeIds){
      if(!("integer" %in% class(eIds))){
        return("Non integer edge ids list!")
      }
    }
    return(TRUE)
  }
)

setGeneric(
  name = "getEdgelist",
  def = function(edgespool, id){
    standardGeneric("getEdgelist")
  }
)

#' EdgesPool class methods
#'
#' \code{getEdgelist} retrieves a single edgelist among all pooled ones, as an Edgelist object\cr
#' \code{Edgelist + Edgelist} allows to concatenate two Edgelist objects into an EdgesPool object of size 2\cr
#' \code{EdgesPool + EdgesPool} allows to concatenate two EdgesPool with compatible node name sets\cr
#' \code{EdgesPool[[id]]} allows to subset an EdgesPool object to retrieve a single edgelist, using the list element operator (== getEdgelist)\cr
#' \code{EdgesPool[ids]} alows to retrieve a subset of an EdgesPool object, by giving the indices of edgelists to keep, as a new EdgesPool object\cr
#' \code{savePool} allows to save an EdgesPool object into a file\cr
#' \code{loadPool} allows to load an EdgesPool object from a file
#' 
#' @param edgespool,x EdgesPool objects
#' @param id,i,j integer indices
#' @param e1,e2 Edgelist or EdgesPool objects
#' 
#' @rdname EdgesPool-methods
#' @include edgelists.R
#' @seealso \code{\link{EdgesPool-class}}, \code{\link{Edgelist-class}}
#'
#' @examples
#' #Set a common nodes space
#' nodes = c("A.t.", "B.t.", "C.t.", "A.t.1.", "B.t.1.", "C.t.1.")
#' #Define some edgelists with edge id vectors
#' edgelist1 = c(4, 7, 9, 12)
#' edgelist2 = c(2, 5, 8, 12)
#' 
#' #Build an EdgesPool object, either from scratch or from intermediate Edgelist objects
#' edgesPool = EdgesPool(nodes = nodes, edgeIds = list(edgelist1, edgelist2))
#' edgesPool = Edgelist(nodes = nodes, edgeIds = edgelist1) + Edgelist(nodes = nodes, edgeIds = edgelist2)
#' 
#' #Retrieve the second edgelist from the pool (two ways, with similar results: el2 == edgelist2)
#' el2 = getEdgelist(edgesPool, 2)
#' el2 = edgesPool[[2]]
#' 
#' #Build a new EdgesPool by reverting the order of edgelists inside it
#' revertedEdgesPool = edgesPool[2:1]
#'
#' @export
setMethod(
  f = "getEdgelist",
  signature = c("EdgesPool", "numeric"),
  definition=function(edgespool, id){
    Edgelist(nodes = edgespool@nodes, edgeIds = edgespool@edgeIds[[id]])
  }
)

#' @rdname EdgesPool-methods
#' @export
"+.Edgelist" = function(e1, e2){
  stopifnot(length(e1@nodes) == length(e2@nodes) && e1@nodes == e2@nodes)
  EdgesPool(nodes = e1@nodes, edgeIds = list(e1@edgeIds, e2@edgeIds))
}

#' @rdname EdgesPool-methods
#' @export
"+.EdgesPool" = function(e1, e2){
  stopifnot(length(e1@nodes) == length(e2@nodes) && e1@nodes == e2@nodes)
  EdgesPool(nodes = e1@nodes, edgeIds = c(e1@edgeIds, e2@edgeIds))
}

#Allows to use [[i]] operation to access a specific edgelist in the pool
#' @rdname EdgesPool-methods
#' @export
"[[.EdgesPool" = function(x, i, j = NULL){
  stopifnot(length(i) == 1)
  getEdgelist(x, i)
}

#Allows to use [[i]] operation to access a specific edgelist in the pool
#' @rdname EdgesPool-methods
#' @export
"[.EdgesPool" = function(x, i, j = NULL){
  EdgesPool(nodes = x@nodes, edgeIds = lapply(i, function(id){
    getEdgelist(x, id)@edgeIds
  }))
}

#' @rdname EdgesPool-methods
#' @export
savePool = function(x, filename){
  edgesPool = x
  save(edgesPool, file = filename)
}

#' @rdname EdgesPool-methods
#' @export
loadPool = function(filename){
  load(filename)
  return(edgesPool)
}