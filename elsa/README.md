#Getting Started

##Build a pool of M original dataset samples, each of size N (sample with replacement)

```
dataPool = getDataPerturbationPool(dataset, N, M, parallelCluster)
```

##Compute partially weighted graphs pool with random edges sampling, with an edges sampling ratio of E

```
fullEL = getAdmissibleEdgeSpace(dataset, TRUE, parentIds)
graphsPool = getWeightedGraphsPool(dataset, dataPool, E, fullEL, symmetricEdgeSampling, logThresholdPre, score, parentIds, parallelCluster),
```

##Learn arborescence model components pool from these graphs

```
components = learnComponentsPool(graphsPool, fullEL, toMaximize, learnFun = Learning.arborescence.fromWeightedGraph, reverse = FALSE, parallelCluster = parallelCluster)
```

##Post processing components via pruning edges with unsignificant scores (here, the ones under 0)

```
components = pruningComponentsFunction(components, 0, TRUE)
```

##Combine the M components

```
compositeModel = combineComponentsInPool(components, M, 1, function(count){count / m})
```

##Learn both best and worst component sets, before aggregating

```
topComponents = learnComponentsPool(graphsPool, fullEL, toMaximize, learnFun = Learning.arborescence.fromWeightedGraph, reverse = FALSE, parallelCluster = parallelCluster)
topComponents = pruningComponentsFunction(topComponents, 0, toMaximize)
bottomComponents = learnComponentsPool(graphsPool, fullEL, !toMaximize, learnFun = Learning.arborescence.fromWeightedGraph, reverse = FALSE, parallelCluster = parallelCluster)
bottomComponents = pruningComponentsFunction(bottomComponents, 0, !toMaximize)
mergedComponents = pairComponentPools(topComponents, bottomComponents, ComponentsAggregator.concat)
compositeModel = combineComponentsInPool(mergedComponents, M, 1, function(count){count / m})
```