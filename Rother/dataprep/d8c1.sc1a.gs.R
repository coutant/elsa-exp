nameD8C1SC1ATrueVec = function(cell, ligand, dataDirName, trueVecDirName){
  lcell = tolower(cell)
  ucell = toupper(cell)

  dataFileName = paste("d8c1.invivo", lcell, ligand, "dyn.tsv", sep = ".")
  dataFullName = paste0(dataDirName, "/", dataFileName)

  trueVecFileName = paste0("TrueVec_", ucell, "_", ligand, ".csv")
  trueVecFullName = paste0(trueVecDirName, "/", trueVecFileName)

  outputFileName = paste0("Named_TrueVec_", ucell, "_", ligand, ".csv")
  outputFullName = paste0(trueVecDirName, "/", outputFileName)

  cnames = colnames(read.table(dataFullName, header = TRUE, stringsAsFactors = FALSE, check.names = FALSE))
  cnames = undyn(cnames[1:(length(cnames)/2)])
  print(paste0("CNames : ", cnames))

  tvec = as.character(read.table(trueVecFullName, sep = ",", stringsAsFactor = FALSE))
  print(tvec)
  validCols = which(!(tvec == "NaN"))
  print(validCols)
  names(tvec)[validCols] = cnames

  output = data.frame(names(tvec)[validCols], tvec[validCols], stringsAsFactors = FALSE)
  print(head(output))
  output = rbind(output[which(output[,2] == "1"),], output[which(output[,2] == "0"),])

  write.table(output, outputFullName, col.names = FALSE, row.names = FALSE)
}

dataDirName = "data/dream"
trueVecDirName = "data/dream/raw/invivo-gs"

for(cell in getD8C1InVivoCellLines()){
  for(ligand in getD8C1InVivoLigands()){
    print(paste0(cell, "-", ligand))
    nameD8C1SC1ATrueVec(cell, ligand, dataDirName, trueVecDirName)
  }
}
