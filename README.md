# elsa-exp

## Instructions to reproduce experiments

1. Please Go to https://www.synapse.org/ if you do not have a Synapse account yet to get credentials (required for dreamtools evaluation). 
2. Build Docker container with following command line (with working directory being the elsa-exp folder): `docker build -t elsaexp --build-arg SYNLOGIN='mysynapselogin' --build-arg SYNPWD='mysynapsepwd' .` adequately changing `"mysynapselogin"` and `"mysynapsepwd"` values to fit your own credentials.
3. Run Docker container in interactive mode: `docker run -it elsa-exp`
4. Launch experiments from within the container: `Rscript exp_d8c1_sc1a.R` or `Rscript exp_d8c1_sc1b.R` for default parameters (the paper ones). Other parameters can be tested for by appending them after the R script filename

